import { Subject } from "rxjs";
const subject = new Subject();

export const MintButtonService = {
    startLoading: () => subject.next(false),
    stopLoading: () => subject.next(true),
    loadingState:() => subject.asObservable()
}
