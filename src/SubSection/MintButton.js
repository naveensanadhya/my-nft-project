import React, { useEffect, useState } from "react";
import { MintButtonService } from "../Services";

export const MintButton = React.forwardRef((props, ref) => {
  const [active, setActive] = useState();
  useEffect(() => {
    setActive(true);
    return () => {
      setActive(true);
    };
  }, []);

  MintButtonService.loadingState().subscribe((message) => {
    setActive(message);
  });

  return active && props && props !== "" ? (
    <>
      <button
        type={"submit"}
        className={props.className || "btn btn-di mt-3 wallet-connect"}
        {...props}
      >
        {props.label}
        {props.children}
      </button>
    </>
  ) : (
    <>
      <button className={"btn btn-di mt-3 wallet-connect"} disabled {...props}>
        <span
          className="spinner-border spinner-border-sm me-2"
          role="status"
          aria-hidden="true"
        ></span>
        {"Processing! Please Wait..."}
      </button>
    </>
  );
});
