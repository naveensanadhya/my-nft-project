import React from "react";
import { loaderType } from "../Common";
import { SwitchLoader } from "../Common/AxiosConfiguration";

export const GlobalForm = React.forwardRef((props, ref) => {
  const onSubmit = (event) => {
    event.preventDefault();
    SwitchLoader(loaderType.BUTTON);
    props.onSubmit(event);
  };

  // console.log(props);
  return (
    <>
      {props && props !== "" && (
        <form {...props} onSubmit={(e) => onSubmit(e)}>
          {props.children}
        </form>
      )}
    </>
  );
});
GlobalForm.displayName = "GlobalForm";
