/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from "react";
import { labels, routesList } from "../Common";

const Footer = () => {
  return (
    <>
      <footer className="footer">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-3 footer-logo">
              <a className="navbar-brand" href={routesList.homePage}>
                <h3 className="text-white">My NFT Project</h3>
              </a>
            </div>
            <div className="col-lg-5">
            </div>
            <div className="col-lg-4 text-end footer-social">
              <a
                href="#"
                // target="_blank"
                // rel="noreferrer"
                className="btn btn-tw"
              >
                <i className="bi bi-twitter"></i> Twitter
              </a>
              <a
                href="#"
                // target="_blank"
                // rel="noreferrer"
                className="btn btn-di"
              >
                <i className="bi bi-discord"></i> Discord
              </a>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
