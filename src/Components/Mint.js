/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState, useRef } from "react";
import {
  loaderType,
  manageCounterProcessing,
  metamaskErrorMessages,
  notificationPosition,
  notificationSetting,
  notificationType,
  saleType,
  showNotification,
  StartLoader,
  StopLoader,
} from "../Common";
import {
  getcurrentlyConnectWallet,
  mintNFT,
  getPrivateSaleStatus,
  totalValues,
} from "../Scripts/interact";
import { MintButton } from "../SubSection";

const Mint = () => {
  const [waiting, setWaiting] = useState(false);
  const [wallet, setWallet] = useState({
    address: null,
    balance: null,
    chainId: null,
  });
  const [walletConnected, setWalletConnected] = useState(false);
  const [mintSaleType, setMintSaleType] = useState(saleType.STANDARD);
  const [quantity, setQuantity] = useState(1);
  const [totalAmount, setTotalAmount] = useState(0);
  const [maxValues, setMaxValues] = useState({
    maxMints: 0,
    totalRemaining: 0,
    maxPerMint: 0,
    price: null,
  });
  const quantityRef = useRef();

  useEffect(async () => {
    const { status, address, walletBalance, chainId } =
      await getcurrentlyConnectWallet();

    if (status) {
      setWalletConnected(true);
    } else {
      setWalletConnected(false);
      setMaxValues({
        maxMints: 0,
        totalRemaining: 0,
        maxPerMint: 0,
        price: null,
      });
    }

    getPrivateSaleStatusFromContract();

    setWallet(() => {
      return { address: address, balance: walletBalance, chainId: chainId };
    });
  }, [wallet.address]);

  useEffect(async () => {
    try {
      setWaiting(true);
      let response = await totalValues();
      if (response && Object.keys(response).length > 0) {
        setWaiting(false);
      }
      setTotalValues(response);
    } catch (error) {
      console.log(error);
      showNotification(
        notificationType.DANGER,
        metamaskErrorMessages.browserNotSupported,
        notificationSetting
      );
      setMaxValues({
        maxMints: 100,
        totalRemaining: 0,
        maxPerMint: 3,
        price: null,
      });
    }
  }, [mintSaleType]);

  useEffect(() => {
    setTotalAmountValues(quantity);
  }, [maxValues.price, mintSaleType]);

  const setTotalAmountValues = (quantity) => {
    let price;
    if (mintSaleType === saleType.PRIVATE_SALE) {
      price = 0;
    } else {
      price = maxValues.price;
    }
    setTotalAmount((parseFloat(quantity) * parseFloat(price)).toFixed(3));
  };

  const setTotalValues = (response) => {
    // console.log("Sale Type: ", mintSaleType);
    if (response !== undefined) {
      if(mintSaleType===saleType.PRIVATE_SALE){
        response.maxPerMint = process.env.REACT_APP_MAX_PER_MINT;
      }
      setMaxValues(response);
    } else {
      showNotification(
        notificationType.DANGER,
        metamaskErrorMessages.browserNotSupported,
        notificationSetting
      );
      setMaxValues({
        maxMints: 100,
        totalRemaining: 0,
        maxPerMint: 3,
        price: null,
      });
    }
  };

  const manageCounter = (buttonType) => {
    const { min, max, value } = quantityRef.current;
    let newValue = manageCounterProcessing(buttonType, min, value, max);
    setQuantity(newValue);
    setTotalAmountValues(newValue);
  };

  const getPrivateSaleStatusFromContract = async (event) => {
    const statusOfPrivateSale = await getPrivateSaleStatus();
    console.log("Private Sale Status", statusOfPrivateSale);
    if (statusOfPrivateSale?.status) {
      setMintSaleType(() => {
        return saleType.PRIVATE_SALE;
      });
    } else {
      setMintSaleType(() => {
        return saleType.STANDARD;
      });
    }
  };

  const mintNFTPressed = async (event) => {
    event.preventDefault();
    StartLoader(loaderType.MINT);
    const { status, message, hash } = await mintNFT(
      quantity,
      totalAmount,
      mintSaleType
    );
    if (status) {
      StopLoader(loaderType.MINT);
      let response = await totalValues();
      setTotalValues(response);
      setQuantity(1);
      setTotalAmountValues(1);
      showNotification(notificationType.SUCCESS, message, {
        title: "Minting Successful",
        duration: 6000,
        notificationPosition: notificationPosition.BOTTOM_RIGHT,
      });
    } else {
      StopLoader(loaderType.MINT);
      showNotification(notificationType.DANGER, message, notificationSetting);
    }
  };

  return (
    <>
      <section className="content">
        <div className="container">
          <div className="banner-sec mint-ban">
            <div className="container">
              <h2 className="text-center">
                MY NFT Project <span className="tknreg">Minting Page</span>
              </h2>
              <div className="banner-item">
                <div className="item-left">
                  <div className="img-art">
                    <img src="images/first.png" alt="" className="img-fluid" />
                  </div>
                </div>
                <div className="item-right text-start">
                  <h3>Please Select Number of NFTs you want to mint.</h3>
                  <p>
                    Each NFT grants you immediate access to an investment
                    automation tool. Historical performance from our investing
                    model beat bitcoin returns by nearly 2x when looking at a
                    four year timeline on Binance. The model began during the
                    end of 2017 and throughout the 2018 great bear market.
                    Disclaimer: Historic returns are not a guarantee of future
                    returns.
                  </p>
                  <form className="min-form">
                    <div className="counter-row">
                      <div className="inner-item">
                        <div className="input-counter mb-3">
                          <button
                            type="button"
                            className="minus-btn"
                            id="minus-btn"
                            onClick={() => manageCounter("minus")}
                          >
                            <i className="bi bi-dash"></i>
                          </button>
                          <span>Qty.</span>
                          <input
                            type="text"
                            id="quantity"
                            name="quantity"
                            className="form-control"
                            value={quantity}
                            ref={quantityRef}
                            min={1}
                            max={maxValues.maxPerMint}
                            onChange={() => {
                              console.log("");
                            }}
                          />
                          <button
                            type="button"
                            className="plus-btn"
                            id="plus-btn"
                            onClick={() => manageCounter("plus")}
                          >
                            <i className="bi bi-plus"></i>
                          </button>
                        </div>
                        <div className="input-form mb-3">
                          <span>Price(ETH)</span>
                          <input
                            type="text"
                            className="form-control"
                            readOnly=""
                            value={totalAmount}
                            onChange={() => {
                              console.log("");
                            }}
                          />
                        </div>
                      </div>
                      {walletConnected && waiting ? (
                        <button
                          className={"btn btn-di mt-3 wallet-connect"}
                          type={"button"}
                          disabled
                        >
                          <span
                            className="spinner-border spinner-border-sm me-2"
                            role="status"
                            aria-hidden="true"
                          ></span>
                          {"Fetching Data! Please Wait..."}
                        </button>
                      ) : (
                        walletConnected && (
                          <MintButton
                            type={"button"}
                            className={"btn btn-di mt-3 wallet-connect"}
                            onClick={mintNFTPressed}
                          >
                            Mint!
                          </MintButton>
                        )
                      )}
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Mint;
