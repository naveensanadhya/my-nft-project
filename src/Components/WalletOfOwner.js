/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { getWalletOfOwner } from "../Scripts/interact";

const WalletOfOwner = () => {
  const [addressData, setaddressData] = useState({});
  const [dataFetching, setDataFetching] = useState(true);
  useEffect(() => {
    const params = new URLSearchParams(window.location.search);

    let address = params.get("address");

    if (address !== null) {
      setaddressData(() => {
        return { wallet_address: address, status: 0 };
      });
    }
  }, []);

  useEffect(async () => {
    if (addressData.wallet_address !== undefined) {
      console.log("aadde: ", addressData.wallet_address);
      await getWalletofOwnerDetails(addressData.wallet_address);
    }
    return () => {
      setaddressData({});
    };
  }, [addressData.wallet_address]);

  const getWalletofOwnerDetails = async (address) => {
    const response = await getWalletOfOwner(address);
    let token;
    if (response?.walletOfOwnerArray?.length > 0) {
      token = 1;
    } else {
      token = 0;
    }
    setaddressData({ ...addressData, status: token });
    setDataFetching(false);
  };
  return (
    <>
      {!dataFetching && (
        <span id={"statusOfAddress"}>{addressData.status}</span>
      )}
    </>
  );
};

export default WalletOfOwner;
