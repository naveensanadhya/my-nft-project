/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import {
  labels,
  metamaskErrorMessages,
  notificationPosition,
  notificationSetting,
  notificationType,
  routesList,
  showNotification,
} from "../Common";
import {
  connectWallet,
  // getBalanceOf,
  getcurrentlyConnectChain,
  getcurrentlyConnectWallet,
  getWalletOfOwner,
} from "../Scripts/interact";

const Header = () => {
  const [isStripClosed, setIsStripClosed] = useState(false);
  const [wallet, setWallet] = useState({
    address: null,
    balance: null,
    chainId: null,
  });
  const [walletConnected, setWalletConnected] = useState(false);
  const [walletOfOwner, setWalletOfOwner] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const { ethereum } = window;
    if (ethereum) {
      setIsStripClosed(() => {
        return true;
      });
    } else {
      setIsStripClosed(() => {
        return false;
      });
    }
  }, []);

  useEffect(async () => {
    const { status, address, walletBalance, chainId } =
      await getcurrentlyConnectWallet();

    if (status) {
      setWalletConnected(true);
    } else {
      setWalletConnected(false);
      if (window.location.pathname === routesList.mint) {
        navigate(routesList.homePage);
      }
    }
    setWallet(() => {
      return { address: address, balance: walletBalance, chainId: chainId };
    });

    monitorWalletChange();
    if (wallet?.address) {
      getWalletofOwnerDetails();
    }

    await getcurrentlyConnectChain();
  }, [wallet.address]);

  const monitorWalletChange = async () => {
    const { ethereum } = window;
    if (ethereum) {
      try {
        const connectAddresses = await ethereum.on(
          "accountsChanged",
          (accounts) => {
            if (accounts.length > 0) {
              setWallet(() => {
                return { address: accounts[0], balance: null, chainId: null };
              });
            } else {
              setWallet({});
            }
          }
        );
        // console.log("Selected Address: ", connectAddresses?.selectedAddress);
      } catch (error) {
        return {
          status: false,
          address: "",
          message: error.message,
        };
      }
    } else {
      return {
        status: false,
        address: "",
        message: (
          <span>
            <p>
              {" "}
              🦊{" "}
              <a
                target="_blank"
                rel="noreferrer"
                href={`https://metamask.io/download.html`}
              >
                You must install Metamask, a virtual Ethereum wallet, in your
                browser.
              </a>
            </p>
          </span>
        ),
      };
    }
  };

  const connectWalletPressed = async () => {
    const response = await connectWallet();

    if (response.status) {
      setWalletConnected(true);
    } else {
      console.log("Else");
      setWalletConnected(false);
      showNotification(
        notificationType.DANGER,
        response.message,
        notificationSetting
      );
    }
    setWallet({
      address: response.address,
      balance: response.walletBalance,
      chainId: response.chainId,
    });
  };

  const getWalletofOwnerDetails = async () => {
    const response = await getWalletOfOwner(wallet?.address);
    if (response.status) {
      setWalletOfOwner(() => {
        return response?.walletOfOwnerArray;
      });
    }
  };

  // const getDetailsofBalance = async () => {
  //   const response = await getBalanceOf(wallet?.address);
  //   console.log(`Balance is: ${response}ETH`);
  // };

  // const togglePassword = (fieldType) => {
  //   if (fieldType === "password") {
  //     setPasswordShown(passwordShown ? false : true);
  //   } else if (fieldType === "confirmPassword") {
  //     setConfPasswordShown(confPasswordShown ? false : true);
  //   }
  // };

  return (
    <>
      <header>
        <div className="topTeam">
          {!isStripClosed && (
            <div className="stripmint">
              You need Metamask to interact with this site.{" "}
              <a
                href={"https://metamask.io/download/"}
                target={"_blank"}
                rel={"noreferrer"}
              >
                Click here to install.
              </a>
              <a
                className="closeStrip clickable"
                onClick={() => setIsStripClosed(true)}
              >
                <i className="bi bi-x-lg"></i>
              </a>
            </div>
          )}
          <nav className="navbar navbar-expand-lg navbar-light">
            <div className="container">
              <a className="navbar-brand" href={routesList.homePage}>
                <h3 className="text-white">My NFT Project</h3>
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <button
                  className="navbar-toggler"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent"
                  aria-expanded="true"
                  aria-label="Toggle navigation"
                >
                  {" "}
                  X
                </button>
                <ul className="navbar-nav ms-auto">
                  <li className="nav-item">
                    {!walletConnected ? (
                      <button
                        type="button"
                        className="nav-link btn btn-purple"
                        onClick={connectWalletPressed}
                      >
                        {labels.connect_wallet}
                      </button>
                    ) : (
                      <Link
                        className="nav-link btn btn-purple" //onClick={mintNFT}>
                        to={routesList.mint}
                      >
                        {labels.mint}
                      </Link>
                    )}
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </header>
    </>
  );
};

export default Header;
