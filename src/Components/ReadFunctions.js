/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState } from "react";
import { Accordion } from "react-bootstrap";
import {
  loaderType,
  metamaskErrorMessages,
  notificationSetting,
  notificationType,
  showNotification,
  StartLoader,
  StopLoader,
} from "../Common";
import { GlobalInput } from "../Global/GlobalInput";
import {
  getApproved,
  getBalanceOf,
  getOwnerOf,
  getSupportsInterfaceStatus,
  getTeamListed,
  getTokenByIndex,
  getTokenOfOwnerByIndex,
  getTokenURI,
  getWalletOfOwner,
  isApprovedForAll,
  isWhitelisted,
} from "../Scripts/interact";
import { MintButton } from "../SubSection";

export const ReadFunctions = React.forwardRef(
  (
    {
      walletConnected,
      price,
      baseExtension,
      baseURI,
      maxGenCount,
      maxPerMint,
      maxMints,
      contractName,
      notRevealedURI,
      ownerAddress,
      paused,
      privateSale,
      publicSale,
      renounceOwnership,
      revealed,
      symbol,
      totalSupply,
      goldMinted,
      silverMinted,
      thronePassSale,
    },
    ref
  ) => {
    const [form, setForm] = useState({});

    const getBalanceOfFromContract = async () => {
      StartLoader(loaderType.MINT);
      let setBalance = "";
      setForm((form) => {
        return { ...form, balance: "" };
      });
      const response = await getBalanceOf(form?.balanceOfInput);
      //console.log("response", response);
      if (response?.status) {
        StopLoader(loaderType.MINT);
        setBalance = response?.balance;
      } else {
        StopLoader(loaderType.MINT);
        showNotification(
          notificationType.DANGER,
          `${metamaskErrorMessages.canNotGetError}Balance ${metamaskErrorMessages.dueTo}${metamaskErrorMessages.seeConsole}`,
          notificationSetting
        );
      }
      setForm((form) => {
        return { ...form, balance: setBalance };
      });
    };

    const getApprovedFromContract = async () => {
      StartLoader(loaderType.MINT);
      let setApproved = "";
      setForm((form) => {
        return { ...form, approvedAddress: "" };
      });
      const response = await getApproved(form?.getApprovedInput);
      //console.log("response", response);
      if (response?.status) {
        StopLoader(loaderType.MINT);
        setApproved = response?.address;
      } else {
        StopLoader(loaderType.MINT);
        showNotification(
          notificationType.DANGER,
          `${metamaskErrorMessages.canNotGetError}Approved ${metamaskErrorMessages.dueTo}${metamaskErrorMessages.seeConsole}`,
          notificationSetting
        );
      }
      setForm((form) => {
        return { ...form, approvedAddress: setApproved };
      });
    };

    const getApprovedForAllFromContract = async () => {
      StartLoader(loaderType.MINT);
      let setApprovedForAll = "";
      setForm((form) => {
        return { ...form, approvedAddress: "" };
      });
      const response = await isApprovedForAll(
        form?.approvedForAllOwnerAddress,
        form?.approvedForAllOperatorAddress
      );
      // console.log("response", response);
      if (response?.status) {
        StopLoader(loaderType.MINT);
        setApprovedForAll = response?.isApprovedForAll;
      } else {
        StopLoader(loaderType.MINT);
        showNotification(
          notificationType.DANGER,
          `${metamaskErrorMessages.canNotGetError}Approved ${metamaskErrorMessages.dueTo}${metamaskErrorMessages.seeConsole}`,
          notificationSetting
        );
      }
      setForm((form) => {
        return { ...form, isApprovedForAll: setApprovedForAll };
      });
    };

    const getOwnerOfFromContract = async () => {
      StartLoader(loaderType.MINT);
      let setOwnerOf = "";
      setForm((form) => {
        return { ...form, ownerOf: "" };
      });
      const response = await getOwnerOf(form?.ownerOfInput);
      //console.log("response", response);
      if (response?.status) {
        StopLoader(loaderType.MINT);
        setOwnerOf = response?.ownerOfToken;
      } else {
        StopLoader(loaderType.MINT);
        showNotification(
          notificationType.DANGER,
          `${metamaskErrorMessages.canNotGetError}Owner ${metamaskErrorMessages.dueTo}${metamaskErrorMessages.seeConsole}`,
          notificationSetting
        );
      }
      setForm((form) => {
        return { ...form, ownerOf: setOwnerOf };
      });
    };

    // const getSupportsInterfaceFromContract = async () => {
    //   const response = await getSupportsInterfaceStatus(form?.interfaceIdInput);
    //   //console.log("response", response);
    //   if (response?.status) {
    //     setForm({
    //       ...form,
    //       supportsInterface: response?.supportsInterfaceStatus,
    //     });
    //   } else {
    //     showNotification(
    //       notificationType.DANGER,
    //       response?.error.message,
    //       notificationSetting
    //     );
    //   }
    // };

    const getTeamListedFromContract = async () => {
      StartLoader(loaderType.MINT);
      let setTeamListed = "";
      setForm((form) => {
        return { ...form, teamListed: "" };
      });
      const response = await getTeamListed(form?.teamListedInput);
      // console.log("response", response);
      if (response?.status) {
        StopLoader(loaderType.MINT);
        setTeamListed = response?.teamListed;
      } else {
        StopLoader(loaderType.MINT);
        showNotification(
          notificationType.DANGER,
          `${metamaskErrorMessages.canNotGetError}Teamlisted ${metamaskErrorMessages.dueTo}${metamaskErrorMessages.seeConsole}`,
          notificationSetting
        );
      }
      setForm((form) => {
        return { ...form, teamListed: setTeamListed };
      });
    };

    const getTokenByIndexFromContract = async () => {
      StartLoader(loaderType.MINT);
      let setTokenByIndex = "";
      setForm((form) => {
        return { ...form, tokenByIndex: "" };
      });
      const response = await getTokenByIndex(form?.tokenByIndexInput);
      // console.log("response", response);
      if (response?.status) {
        StopLoader(loaderType.MINT);
        setTokenByIndex = response?.tokenByIndex;
      } else {
        StopLoader(loaderType.MINT);
        showNotification(
          notificationType.DANGER,
          `${metamaskErrorMessages.canNotGetError}Token ${metamaskErrorMessages.dueTo}${metamaskErrorMessages.seeConsole}`,
          notificationSetting
        );
      }
      setForm((form) => {
        return { ...form, tokenByIndex: setTokenByIndex };
      });
    };

    const getTokenOfOwnerIndexFromContract = async () => {
      StartLoader(loaderType.MINT);
      let setTokenOfOwnerByIndex = "";
      setForm((form) => {
        return { ...form, tokenOfOwnerByIndex: "" };
      });
      const response = await getTokenOfOwnerByIndex(
        form?.tokenOfOwnerAddress,
        form?.tokenOfOwnerIndex
      );
      // console.log("response", response);
      if (response?.status) {
        StopLoader(loaderType.MINT);
        setTokenOfOwnerByIndex = response?.tokenOfOwnerByIndex;
      } else {
        StopLoader(loaderType.MINT);
        showNotification(
          notificationType.DANGER,
          `${metamaskErrorMessages.canNotGetError}Token ${metamaskErrorMessages.dueTo}${metamaskErrorMessages.seeConsole}`,
          notificationSetting
        );
      }
      setForm((form) => {
        return { ...form, tokenOfOwnerByIndex: setTokenOfOwnerByIndex };
      });
    };

    const getTokenURIFromContract = async () => {
      StartLoader(loaderType.MINT);
      let setTokenURI = "";
      setForm((form) => {
        return { ...form, tokenURI: "" };
      });
      const response = await getTokenURI(form?.tokenUriInput);
      // console.log("response", response);
      if (response?.status) {
        StopLoader(loaderType.MINT);
        setTokenURI = response?.tokenURI;
      } else {
        StopLoader(loaderType.MINT);
        showNotification(
          notificationType.DANGER,
          `${metamaskErrorMessages.canNotGetError}Token URI ${metamaskErrorMessages.dueTo}${metamaskErrorMessages.seeConsole}`,
          notificationSetting
        );
      }
      setForm((form) => {
        return { ...form, tokenURI: setTokenURI };
      });
    };

    const getWalletOfOwnerFromContract = async () => {
      StartLoader(loaderType.MINT);
      let setWalletOfOwnerArray = "";
      setForm((form) => {
        return { ...form, walletOfOwnerArray: "" };
      });
      const response = await getWalletOfOwner(form?.walletOfOwnerInput);
      // console.log("response", response);
      if (response?.status) {
        StopLoader(loaderType.MINT);
        setWalletOfOwnerArray =
          response?.walletOfOwnerArray.length > 0
            ? response?.walletOfOwnerArray.toString()
            : "";
      } else {
        StopLoader(loaderType.MINT);
        showNotification(
          notificationType.DANGER,
          `${metamaskErrorMessages.canNotGetError}Wallet Details ${metamaskErrorMessages.dueTo}${metamaskErrorMessages.seeConsole}`,
          notificationSetting
        );
      }
      setForm((form) => {
        return { ...form, walletOfOwnerArray: setWalletOfOwnerArray };
      });
    };

    const getWhitelistedStatusFromContract = async () => {
      StartLoader(loaderType.MINT);
      let setWhitelisted = "";
      setForm((form) => {
        return { ...form, whitelisted: "" };
      });
      const response = await isWhitelisted(form?.whitelistedInput);
      // console.log("response", response);
      if (response?.status) {
        StopLoader(loaderType.MINT);
        setWhitelisted = response?.whitelisted;
      } else {
        StopLoader(loaderType.MINT);
        showNotification(
          notificationType.DANGER,
          `${metamaskErrorMessages.canNotGetError}Whitelisted Status ${metamaskErrorMessages.dueTo}${metamaskErrorMessages.seeConsole}`,
          notificationSetting
        );
      }
      setForm((form) => {
        return { ...form, whitelisted: setWhitelisted };
      });
    };

    //Setter Functions
    const handleInputChange = (event, data) => {
      setForm({ ...form, [data.name]: data.value });
    };

    return (
      <>
        <Accordion defaultActiveKey="price">
          <Accordion.Item eventKey="price">
            <Accordion.Header>Price</Accordion.Header>
            <Accordion.Body>
              <span>
                <a
                  href={`https://etherscan.io/unitconverter?wei=${price}`}
                  target={"_blank"}
                  rel="noreferrer"
                >
                  {price}
                </a>
                &nbsp;<span className="text-secondary">uint256</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="balanceOf">
            <Accordion.Header>balanceOf</Accordion.Header>
            <Accordion.Body>
              <h6 className="mb-3">{`See {IERC721-balanceOf}.`}</h6>
              <br />
              <GlobalInput
                label={"owner (address)"}
                placeholder={"owner (address)"}
                id={"balanceOfInput"}
                name={"balanceOfInput"}
                value={form?.balanceOfInput}
                onChange={(event, data) => handleInputChange(event, data)}
              />
              <MintButton
                type={"button"}
                className={"btn btn-di mt-3 wallet-connect"}
                onClick={getBalanceOfFromContract}
              >
                Query!
              </MintButton>
              {form?.balance && form?.balance !== "" && (
                <>
                  <hr />
                  <span>Balance: {form?.balance}</span>
                </>
              )}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="baseURI">
            <Accordion.Header>baseURI</Accordion.Header>
            <Accordion.Body>
              <span>
                {baseURI}&nbsp;<span className="text-secondary">string</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="getApproved">
            <Accordion.Header>getApproved</Accordion.Header>
            <Accordion.Body>
              <h6 className="mb-3">{`See {IERC721-getApproved}.`}</h6>
              <br />
              <GlobalInput
                label={"tokenId (uint256)"}
                placeholder={"tokenId (uint256)"}
                id={"getApprovedInput"}
                name={"getApprovedInput"}
                value={form?.getApprovedInput}
                onChange={(event, data) => handleInputChange(event, data)}
              />
              <MintButton
                type={"button"}
                className={"btn btn-di mt-3 wallet-connect"}
                onClick={getApprovedFromContract}
              >
                Query!
              </MintButton>
              {form?.approvedAddress && form?.approvedAddress !== "" && (
                <>
                  <hr />
                  <span>Approved Address: {form?.approvedAddress}</span>
                </>
              )}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="isApprovedForAll">
            <Accordion.Header>isApprovedForAll</Accordion.Header>
            <Accordion.Body>
              <GlobalInput
                label={"owner (address)"}
                placeholder={"owner (address)"}
                id={"approvedForAllOwnerAddress"}
                name={"approvedForAllOwnerAddress"}
                value={form?.approvedForAllOwnerAddress}
                onChange={(event, data) => handleInputChange(event, data)}
              />
              <br />
              <GlobalInput
                label={"operator (address)"}
                placeholder={"operator (address)"}
                id={"approvedForAllOperatorAddress"}
                name={"approvedForAllOperatorAddress"}
                value={form?.approvedForAllOperatorAddress}
                onChange={(event, data) => handleInputChange(event, data)}
              />
              <MintButton
                type={"button"}
                className={"btn btn-di mt-3 wallet-connect"}
                onClick={getApprovedForAllFromContract}
              >
                Query!
              </MintButton>
              {form?.isApprovedForAll !== undefined &&
                form?.isApprovedForAll !== "" && (
                  <>
                    <hr />
                    <span>
                      Approved: {form?.isApprovedForAll ? "true" : "false"}
                    </span>
                  </>
                )}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="maxPerMintTokens">
            <Accordion.Header>maxPerMintTokens</Accordion.Header>
            <Accordion.Body>
              <span>
                {maxPerMint}&nbsp;
                <span className="text-secondary">uint256</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          {/* <Accordion.Item eventKey="maxSupply">
            <Accordion.Header>maxSupply</Accordion.Header>
            <Accordion.Body>
              <span>
                {maxMints}&nbsp;<span className="text-secondary">uint256</span>
              </span>
            </Accordion.Body>
          </Accordion.Item> */}
          <Accordion.Item eventKey="name">
            <Accordion.Header>name</Accordion.Header>
            <Accordion.Body>
              <h6 className="mb-3">{`See {IERC721Metadata-name}.`}</h6>
              <br />
              <span>
                {contractName}&nbsp;
                <span className="text-secondary">string</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="notRevealedUri">
            <Accordion.Header>notRevealedUri</Accordion.Header>
            <Accordion.Body>
              <span>
                {notRevealedURI}&nbsp;
                <span className="text-secondary">string</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="owner">
            <Accordion.Header>owner</Accordion.Header>
            <Accordion.Body>
              <h6 className="mb-3">{`Returns the address of the current owner.`}</h6>
              <br />
              <span>
                <a
                  href={`https://etherscan.io/address/${ownerAddress}`}
                  target={"_blank"}
                  rel="noreferrer"
                >
                  {ownerAddress}
                </a>
                &nbsp;
                <span className="text-secondary">address</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="ownerOf">
            <Accordion.Header>ownerOf</Accordion.Header>
            <Accordion.Body>
              <h6 className="mb-3">{`See {IERC721-ownerOf}.`}</h6>
              <br />
              <GlobalInput
                label={"tokenId (uint256)"}
                placeholder={"tokenId (uint256)"}
                id={"ownerOfInput"}
                name={"ownerOfInput"}
                value={form?.ownerOfInput}
                onChange={(event, data) => handleInputChange(event, data)}
              />
              <MintButton
                type={"button"}
                className={"btn btn-di mt-3 wallet-connect"}
                onClick={getOwnerOfFromContract}
              >
                Query!
              </MintButton>
              {form?.ownerOf && form?.ownerOf !== "" && (
                <>
                  <hr />
                  <span>Onwer's Address: {form?.ownerOf}</span>
                </>
              )}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="paused">
            <Accordion.Header>paused</Accordion.Header>
            <Accordion.Body>
              <span>
                {paused ? "true" : "false"}&nbsp;
                <span className="text-secondary">bool</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="goldMinted">
            <Accordion.Header>goldMinted</Accordion.Header>
            <Accordion.Body>
              <span>
                {goldMinted ? "true" : "false"}&nbsp;
                <span className="text-secondary">bool</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="silverMinted">
            <Accordion.Header>silverMinted</Accordion.Header>
            <Accordion.Body>
              <span>
                {silverMinted ? "true" : "false"}&nbsp;
                <span className="text-secondary">bool</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="privateSale">
            <Accordion.Header>privateSale</Accordion.Header>
            <Accordion.Body>
              <span>
                {privateSale ? "true" : "false"}&nbsp;
                <span className="text-secondary">bool</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="publicSale">
            <Accordion.Header>publicSale</Accordion.Header>
            <Accordion.Body>
              <span>
                {publicSale ? "true" : "false"}&nbsp;
                <span className="text-secondary">bool</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="revealed">
            <Accordion.Header>revealed</Accordion.Header>
            <Accordion.Body>
              <span>
                {revealed ? "true" : "false"}&nbsp;
                <span className="text-secondary">bool</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          {/* <Accordion.Item eventKey="supportsInterface">
            <Accordion.Header>supportsInterface</Accordion.Header>
            <Accordion.Body>
              <h6 className="mb-3">{`See {IERC165-supportsInterface}.`}</h6>
              <br />
              <GlobalInput
                label={"interfaceId (bytes4)"}
                placeholder={"interfaceId (bytes4)"}
                id={"interfaceIdInput"}
                name={"interfaceIdInput"}
                value={form?.interfaceIdInput}
                onChange={(event, data) => handleInputChange(event, data)}
              />
              <MintButton
                type={"button"}
                className={"btn btn-di mt-3 wallet-connect"}
                onClick={getSupportsInterfaceFromContract}
              >
                Query!
              </MintButton>
            </Accordion.Body>
          </Accordion.Item> */}
          <Accordion.Item eventKey="symbol">
            <Accordion.Header>symbol</Accordion.Header>
            <Accordion.Body>
              <h6 className="mb-3">{`See {IERC721Metadata-symbol}.`}</h6>
              <br />
              <span>
                {symbol}&nbsp;<span className="text-secondary">string</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="teamListed">
            <Accordion.Header>teamListed</Accordion.Header>
            <Accordion.Body>
              <GlobalInput
                label={"<input> (address)"}
                placeholder={"<input> (address)"}
                id={"teamListedInput"}
                name={"teamListedInput"}
                value={form?.teamListedInput}
                onChange={(event, data) => handleInputChange(event, data)}
              />
              <MintButton
                type={"button"}
                className={"btn btn-di mt-3 wallet-connect"}
                onClick={getTeamListedFromContract}
              >
                Query!
              </MintButton>
              {form?.teamListed !== undefined && form?.teamListed !== "" && (
                <>
                  <hr />
                  <span>TeamListed: {form?.teamListed}</span>
                </>
              )}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="tokenByIndex">
            <Accordion.Header>tokenByIndex</Accordion.Header>
            <Accordion.Body>
              <h6 className="mb-3">{`See {IERC165-supportsInterface}.`}</h6>
              <br />
              <GlobalInput
                label={"index (uint256)"}
                placeholder={"index (uint256)"}
                id={"tokenByIndexInput"}
                name={"tokenByIndexInput"}
                value={form?.tokenByIndexInput}
                onChange={(event, data) => handleInputChange(event, data)}
              />
              <MintButton
                type={"button"}
                className={"btn btn-di mt-3 wallet-connect"}
                onClick={getTokenByIndexFromContract}
              >
                Query!
              </MintButton>
              {form?.tokenByIndex && form?.tokenByIndex !== "" && (
                <>
                  <hr />
                  <span>Token By Index: {form?.tokenByIndex}</span>
                </>
              )}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="tokenOfOwnerByIndex">
            <Accordion.Header>tokenOfOwnerByIndex</Accordion.Header>
            <Accordion.Body>
              <GlobalInput
                label={"owner (address)"}
                placeholder={"owner (address)"}
                id={"tokenOfOwnerAddress"}
                name={"tokenOfOwnerAddress"}
                value={form?.tokenOfOwnerAddress}
                onChange={(event, data) => handleInputChange(event, data)}
              />
              <br />
              <GlobalInput
                label={"index (uint256)"}
                placeholder={"index (uint256)"}
                id={"tokenOfOwnerIndex"}
                name={"tokenOfOwnerIndex"}
                value={form?.tokenOfOwnerIndex}
                onChange={(event, data) => handleInputChange(event, data)}
              />
              <MintButton
                type={"button"}
                className={"btn btn-di mt-3 wallet-connect"}
                onClick={getTokenOfOwnerIndexFromContract}
              >
                Query!
              </MintButton>
              {form?.tokenOfOwnerByIndex && form?.tokenOfOwnerByIndex !== "" && (
                <>
                  <hr />
                  <span>
                    Token of Owner By Index: {form?.tokenOfOwnerByIndex}
                  </span>
                </>
              )}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="tokenURI">
            <Accordion.Header>tokenURI</Accordion.Header>
            <Accordion.Body>
              <h6 className="mb-3">{`See {IERC165-supportsInterface}.`}</h6>
              <br />
              <GlobalInput
                label={"_tokenId (uint256)"}
                placeholder={"_tokenId (uint256)"}
                id={"tokenUriInput"}
                name={"tokenUriInput"}
                value={form?.tokenUriInput}
                onChange={(event, data) => handleInputChange(event, data)}
              />
              <MintButton
                type={"button"}
                className={"btn btn-di mt-3 wallet-connect"}
                onClick={getTokenURIFromContract}
              >
                Query!
              </MintButton>
              {form?.tokenURI && form?.tokenURI !== "" && (
                <>
                  <hr />
                  <span>Token URI: {form?.tokenURI}</span>
                </>
              )}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="totalSupply">
            <Accordion.Header>totalSupply</Accordion.Header>
            <Accordion.Body>
              <span>
                {totalSupply}&nbsp;
                <span className="text-secondary">uint256</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="walletOfOwner">
            <Accordion.Header>walletOfOwner</Accordion.Header>
            <Accordion.Body>
              <GlobalInput
                label={"_owner (address)"}
                placeholder={"_owner (address)"}
                id={"walletOfOwnerInput"}
                name={"walletOfOwnerInput"}
                value={form?.walletOfOwnerInput}
                onChange={(event, data) => handleInputChange(event, data)}
              />
              <MintButton
                type={"button"}
                className={"btn btn-di mt-3 wallet-connect"}
                onClick={getWalletOfOwnerFromContract}
              >
                Query!
              </MintButton>
              {form?.walletOfOwnerArray && form?.walletOfOwnerArray !== "" && (
                <>
                  <hr />
                  <span>Wallet(tokens): {`[${form?.walletOfOwnerArray}]`}</span>
                </>
              )}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="whitelisted">
            <Accordion.Header>whitelisted</Accordion.Header>
            <Accordion.Body>
              <GlobalInput
                label={"<input> (address)"}
                placeholder={"<input> (address)"}
                id={"whitelistedInput"}
                name={"whitelistedInput"}
                value={form?.whitelistedInput}
                onChange={(event, data) => handleInputChange(event, data)}
              />
              <MintButton
                type={"button"}
                className={"btn btn-di mt-3 wallet-connect"}
                onClick={getWhitelistedStatusFromContract}
              >
                Query!
              </MintButton>
              {form?.whitelisted !== "" && (
                <>
                  <hr />
                  <span>
                    Whitelisted: {form?.whitelisted ? "true" : "false"}
                  </span>
                </>
              )}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="baseExtension">
            <Accordion.Header>baseExtension</Accordion.Header>
            <Accordion.Body>
              <span>
                {baseExtension}&nbsp;
                <span className="text-secondary">uint256</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="maxGenCount">
            <Accordion.Header>maxGenCount</Accordion.Header>
            <Accordion.Body>
              <span>
                {maxGenCount}&nbsp;
                <span className="text-secondary">uint256</span>
              </span>
            </Accordion.Body>
          </Accordion.Item>
          {/* <Accordion.Item eventKey="renounceOwnership">
            <Accordion.Header>renounceOwnership</Accordion.Header>
            <Accordion.Body>
              <span>
                {renounceOwnership}&nbsp;
                <span className="text-secondary">uint256</span>
              </span>
            </Accordion.Body>
          </Accordion.Item> */}
        </Accordion>
      </>
    );
  }
);
